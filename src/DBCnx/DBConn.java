/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DBCnx;

/**
 *
 * @author mjcave Feb 2015
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class DBConn {
    private String dbName;
    private Connection conn;     //public so can be seen for prep statements
    private PreparedStatement pstmt;
    private ResultSet rset;
  
    public DBConn() {
        //constructor
   
    }
   
     
    public void createConnection() {
        // Load JDBC driver & set up connection
        try {
            conn = DriverManager.getConnection  ("jdbc:oracle:thin:@tom.uopnet.plymouth.ac.uk:1522:ORCL12c", "PRDCB", "GreenSwan2015!");             
            }
        catch (SQLException s) {
            System.out.println("PRDCB DBCnxr SQL Error: " + s.toString() + " " 
                    + s.getErrorCode() + " " + s.getSQLState());
            } 
        catch (Exception e) {
            System.out.println("PRDCB DBCnxr Error: " + e.toString() + "   " + e.getMessage());
            }

    }
    
    public void closeConnection()  {
        try{
            conn.close();
            }
        catch (SQLException s) {
            System.out.println("PRDCB DBCnxr SQL Error: " + s.toString() + " " 
                    + s.getErrorCode() + " " + s.getSQLState());
            } 
        catch (Exception e) {
            System.out.println("PRDCB DBCnxr Error: " + e.toString() + e.getMessage());
            }    
    }
    
   
    public ResultSet  execRSSQL(String SQL)  {
        // SQL string passed in, Record Set passed back       
        try {
            createConnection();
            pstmt = conn.prepareStatement(SQL);
            rset = pstmt.executeQuery();
            }
            catch (SQLException s) {
                System.out.println("PRDCB DBCnxr SQL Error: " + s.toString() + " " 
                    + s.getErrorCode() + " " + s.getSQLState());
                } 
            catch (Exception e) {
                System.out.println("PRDCB DBCnxr Error: " + e.toString() + e.getMessage());
                }  

        return rset;
        
    }
    
    
    public void showMTest()  {
    
        String SQL;
        try {
            SQL = "SELECT * FROM MTest";
            pstmt = conn.prepareStatement(SQL);
            rset = pstmt.executeQuery();
            //1 =name 2 =rank 
            while (rset.next()) {
                System.out.println("Name: " + rset.getString(1) + "  Rank:  " + rset.getString(2) + "  SerNo:"  + rset.getString(3));
                }
            }
            catch (SQLException s) {
                System.out.println("PRDCB DBCnxr 1 SQL Error: " + s.toString() + " " 
                    + s.getErrorCode() + " " + s.getSQLState());
                } 
            catch (Exception e) {
                System.out.println("PRDCB DBCnxr 2 Error: " + e.toString() + e.getMessage());
                }        
    }
}