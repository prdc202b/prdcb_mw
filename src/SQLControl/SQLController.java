/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package SQLControl;


import DBCnx.DBConn;

import java.sql.ResultSet;
import java.sql.SQLException;

//LETS entity classes
import LETS_Entity.Category;


/**
 *
 * @author Mike 15 Feb 2015
 * 
 * SQL Control class - will control access to the DB
 *        and contain SQL prepared statements which will do the CRUD 
 *        for the  entity classes- some statements pass back entity classes
 *          and collections. 
 * 
 */

public class SQLController {
    
    private ResultSet rset;
    
    public SQLController (){ 
        // empty constructor to create donor class
    }
   
    
    public void fetchCategories(){
       String SQL;
       DBConn db = new DBConn();
       //get cats into rs
       try {          
            SQL = "SELECT * FROM Categories";
            rset = db.execRSSQL(SQL);
            
            //1 = category
            while (rset.next()) {                     
                //debug only
                System.out.println("Category: " + rset.getString(1));
                
                Category cat = new Category(rset.getString(1));
               
                //debug only
                System.out.println("after printing");
                             
                }                     
            }
            catch (SQLException s) {
                System.out.println("PRDCB SQLController SQL Error: " + s.toString() + " " 
                    + s.getErrorCode() + " " + s.getSQLState());
                } 
            catch (Exception e) {
                System.out.println("PRDCB SQLController Error: " + e.toString() + e.getMessage());
                }  
       
       //tidyup   
        db.closeConnection();       
           
    }
    
}// end class
